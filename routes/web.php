<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();
Auth::routes(['verify' => true]);

Route::get('/', [App\Http\Controllers\HomeController::class, 'index']);
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/profile', [App\Http\Controllers\HomeController::class, 'profile'])->name('profile');
Route::post('/profile/update', [App\Http\Controllers\HomeController::class, 'profile_update'])->name('profile-update');

Route::get('/client', [App\Http\Controllers\ClientHomeController::class, 'index'])->name('client');
Route::post('/client/save', [App\Http\Controllers\ClientHomeController::class, 'save'])->name('client-save');

Route::get('/client/case/{client_id?}', [App\Http\Controllers\ClientHomeController::class, 'index_case'])->name('case');
Route::post('/client/case/save', [App\Http\Controllers\ClientHomeController::class, 'save_case'])->name('case-save');

Route::get('/import', [App\Http\Controllers\HomeController::class, 'import'])->name('import');
Route::post('/import/pasal', [App\Http\Controllers\HomeController::class, 'upload_pasal']);

Route::get('/regulation', [App\Http\Controllers\RegulationController::class, 'index'])->name('regulation');
Route::get('/regulation/{id?}', [App\Http\Controllers\RegulationController::class, 'detail'])->name('regulation-detail');
Route::post('/regulation/search', [App\Http\Controllers\RegulationController::class, 'search'])->name('regulation-search');

Route::get('/prediksi/lawyer', [App\Http\Controllers\PrediksiHargaController::class, 'index'])->name('prediksi-lawyer');
Route::post('/prediksi/lawyer/search', [App\Http\Controllers\PrediksiHargaController::class, 'search'])->name('prediksi-lawyer-search');