<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\CaseController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'
], function ($router) {
    Route::post('/login', [AuthController::class, 'login']);
    Route::post('/register', [AuthController::class, 'register']);
    Route::post('/logout', [AuthController::class, 'logout']);
    Route::post('/refresh', [AuthController::class, 'refresh']);
    Route::get('/user-profile', [AuthController::class, 'userProfile']);    

    // Client
    Route::get('/client', [ClientController::class, 'index']);
    Route::post('/client/store', [ClientController::class, 'store']);
    Route::post('/client/update', [ClientController::class, 'update']);
    Route::get('/client/get/{client_id?}', [ClientController::class, 'get']);
    Route::get('/client/delete/{client_id?}', [ClientController::class, 'delete']);

    // Cases 
    Route::get('/cases/{client_id}', [CaseController::class, 'index']);
    Route::post('/cases/store', [CaseController::class, 'store']);
    Route::post('/cases/update', [CaseController::class, 'update']);
    Route::get('/cases/get/{cases_id?}', [CaseController::class, 'get']);
    Route::get('/cases/delete/{cases_id?}', [CaseController::class, 'delete']);
    Route::post('/cases/store/detail', [CaseController::class, 'storeDetail']);
    Route::post('/cases/update/detail', [CaseController::class, 'updateDetail']);
    Route::post('/cases/store/settlement', [CaseController::class, 'storeSettlement']);
    Route::post('/cases/update/settlement', [CaseController::class, 'updateSettlement']);
    Route::post('/cases/store/session', [CaseController::class, 'storeSession']);
    Route::post('/cases/update/session', [CaseController::class, 'updateSession']);
    Route::post('/cases/store/document', [CaseController::class, 'storeDocument']);
    Route::get('/cases/delete/document/{document_id}', [CaseController::class, 'deleteDocument']);
});
