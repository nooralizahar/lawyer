@extends('layouts.app')
@section('header')
<div class="container">
    <button class="navbar-toggler" onclick="history.back()">
        <i class="fa fa-arrow-left"></i>
    </button>
    <a class="navbar-brand">
        Catatan Pengacara
    </a>
</div>
@section('content')
<div class="container lawyer-content">
    <div class="row justify-content-center">
        <div class="col-md-12" style="padding-bottom: 3rem">
            @if (count($client_case))
            @foreach ($client_case as $item)
            <a href="#" >
                <div class="card">
                    <div class="card-body">
                        <div class="grid-column-2">
                            <div class="column-row">
                                <img src="/user-profile.png" alt="">
                            </div>
                            <div class="column-row">
                                <span><b>Nama</b>: {{ ' '.$item->client->name ?? '' }}</span> <br>
                                <span><b>Tanggal</b>: {{ ' '.date('d M Y' ,strtotime($item->meeting_date)) ?? '' }}</span><br>
                                <span><b>Deskripsi</b>: <br>{{ ' '.$item->description ?? '' }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
            @endforeach
            @else
            <p class="text-center text-secondary">Daftar Klien Kosong</p>
            @endif
        </div>
    </div>
    
    <div class="content-add">
        <div class="mybutton">
            <button class="add" data-bs-toggle="modal" data-bs-target="#exampleModal"><i class="fa fa-plus"></i></button>
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Kasus</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="/client/case/save" method="post">
                <div class="modal-body">
                    @csrf
                    @if (Request::segment(3) != null)
                    <input type="hidden" name="client_id" id="client_id" value="{{ Request::segment(3) }}">
                    @else
                    <label for="meeting_date" class="col-md-4 col-form-label text-md-end"> Pilih Klien </label>
                    <div class="col-md-12">
                        <select name="client_id" class="form-control" required>
                            <option value="" selected disabled>Select Client</option>
                            @foreach ($client as $item)
                            <option value="{{ $item->id ?? '' }}">{{ $item->name ?? '' }}</option>
                            @endforeach
                        </select>
                    </div>
                    
                    @error('client_id')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                    @endif
                    
                    {{-- meet date --}}
                    <div class="form-group">
                        <label for="meeting_date" class="col-md-4 col-form-label text-md-end"> Tanggal Meet </label>
                        
                        <div class="col-md-12">
                            <input id="meeting_date" type="date" class="form-control @error('meeting_date') is-invalid @enderror" name="meeting_date" value="{{ old('meeting_date') }}" required  autofocus>
                            
                            @error('meeting_date')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    
                    {{-- case number --}}
                    {{-- <div class="form-group">
                        <label for="case_number" class="col-md-4 col-form-label text-md-end"> Nomor Kasus </label>
                        
                        <div class="col-md-6">
                            <input id="case_number" type="text" class="form-control @error('case_number') is-invalid @enderror" name="case_number" value="{{ old('case_number') }}" required >
                            
                            @error('case_number')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div> --}}
                    
                    {{-- case date --}}
                    
                    {{-- <div class="form-group">
                        <label for="case_date" class="col-md-4 col-form-label text-md-end"> Tanggal Kasus </label>
                        
                        <div class="col-md-6">
                            <input id="case_date" type="date" class="form-control @error('case_date') is-invalid @enderror" name="case_date" value="{{ old('case_date') }}" required  >
                            
                            @error('case_date')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div> --}}
                    {{-- claim detail --}}
                    
                    {{-- <div class="form-group">
                        <label for="claim_detail" class="col-md-4 col-form-label text-md-end"> Tuntutan Klien </label>
                        
                        <div class="col-md-6">
                            <input id="claim_detail" type="text" class="form-control @error('claim_detail') is-invalid @enderror" name="claim_detail" value="{{ old('claim_detail') }}" required  >
                            
                            @error('claim_detail')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div> --}}
                    
                    {{-- description --}}
                    
                    <div class="form-group">
                        <label for="description" class="col-md-4 col-form-label text-md-end"> Deskripsi </label>
                        
                        <div class="col-md-12">
                            <textarea class="form-control @error('description') is-invalid @enderror" name="description" id="exampleFormControlTextarea1" rows="3"></textarea>
                            
                            @error('description')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@endsection
