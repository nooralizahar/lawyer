@extends('layouts.app')
@section('header')
<div class="container">
    <button class="navbar-toggler" onclick="history.back()">
        <i class="fa fa-arrow-left"></i>
    </button>
    <a class="navbar-brand">
        Daftar Klien
    </a>
</div>
@section('content')
<div class="container lawyer-content">
    <div class="row justify-content-center">
        <div class="col-md-12" style="padding-bottom: 3rem">
            @if (count($client))
            @foreach ($client as $item)
            <a href="/client/case/{{$item->id}}" >
                <div class="card">
                    <div class="card-body">
                        <div class="grid-column-2">
                            <div class="column-row">
                                <img src="/user-profile.png" alt="">
                            </div>
                            <div class="column-row">
                                <span>{{ $item->name ?? '' }}</span>
                                <span>{{ $item->email ?? '' }}</span>
                                <span>{{ $item->phone_number ?? '' }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
            @endforeach
            @else
            <p class="text-center text-secondary">Daftar Klien Kosong</p>
            @endif
        </div>
    </div>
    <div class="content-add">
        <div class="mybutton">
            <button class="add" data-bs-toggle="modal" data-bs-target="#exampleModal"><i class="fa fa-plus"></i></button>
        </div>
    </div>
    
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Klien</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="/client/save" method="POST">
                <div class="modal-body">
                    @csrf
                    <div class="form-group">
                        <label class="col-form-label text-md-end" for="">Nama Lengkap</label>
                        <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label class="col-form-label text-md-end" for="">Email</label>
                        <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label class="col-form-label text-md-end" for="">Nomor Telepon</label>
                        <input type="number" class="form-control @error('phone_number') is-invalid @enderror" name="phone_number" value="{{ old('phone_number') }}" required autocomplete="phone_number" autofocus>
                        @error('phone_number')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label class="col-form-label text-md-end" for="">Alamat</label>
                        <input type="text" class="form-control @error('address') is-invalid @enderror" name="address" value="{{ old('address') }}" required autocomplete="address" autofocus>
                        @error('address')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
@endsection
