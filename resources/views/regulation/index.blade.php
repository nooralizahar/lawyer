@extends('layouts.app')
@section('header')
<div class="container">
    <button class="navbar-toggler" onclick="history.back()">
        <i class="fa fa-arrow-left"></i>
    </button>
    <a class="navbar-brand">
        Daftar Peraturan
    </a>
</div>
@section('content')
<div class="container lawyer-content">
    <div class="row justify-content-center">
        <div class="col-md-12" style="padding-bottom: 3rem">
            @foreach ($pasal as $item)
            <a href="/regulation/{{$item->id}}" >
                <div class="card">
                    <div class="card-body">
                        <div class="grid-column-1">
                            <div class="column-row">
                                <h6>{{ $item->peraturan ?? '' }}</h6>
                                <p>Bab {{ $item->bab ?? '' }} - Pasal {{ $item->pasal ?? '' }}</p>
                                <b>{{ $item->judul ?? '' }}</b>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
            @endforeach
        </div>
    </div>
    <div class="content-add">
        <div class="mybutton">
            <button class="add" data-bs-toggle="modal" data-bs-target="#exampleModal"><i class="fa fa-search"></i></button>
        </div>
    </div>
    
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Cari Peraturan</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="/regulation/search" method="POST">
                <div class="modal-body">
                    @csrf
                    <div class="form-group">
                        <label class="col-form-label text-md-end" for="">Kata Kunci</label>
                        <input type="text" class="form-control" name="keywoard" required autocomplete="keywoard">
                    </div>
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Cari</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
@endsection
