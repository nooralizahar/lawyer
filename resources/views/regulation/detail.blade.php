@extends('layouts.app')
@section('header')
<div class="container">
    <button class="navbar-toggler" onclick="history.back()">
        <i class="fa fa-arrow-left"></i>
    </button>
    <a class="navbar-brand">
        Detail Peraturan
    </a>
</div>
@section('content')
<div class="container lawyer-content">
    <div class="row justify-content-center">
        <div class="col-md-12" style="padding-bottom: 3rem">
            <div class="card">
                <div class="card-body">
                    {!! $row->isi_pasal ?? '' !!}
                </div>
            </div>
        </div>
    </div>
    
</div>

@endsection
@endsection
