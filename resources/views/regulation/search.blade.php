@extends('layouts.app')
@section('header')
<div class="container">
    <button class="navbar-toggler" onclick="history.back()">
        <i class="fa fa-arrow-left"></i>
    </button>
    <a class="navbar-brand">
        Search Peraturan
    </a>
</div>
@section('content')
<div class="container lawyer-content">
    <div class="row justify-content-center">
        <div class="col-md-12" style="padding-bottom: 3rem">
            <div class="card">
                <div class="card-header">
                    keywoard: <b>{{ $keywoard ?? '' }}</b>
                </div>
                <div class="card-body">
                    @foreach ($pasal as $item)
                    <a href="/regulation/{{$item->id}}" >
                        <div class="card">
                            <div class="card-body">
                                <div class="grid-column-1">
                                    <div class="column-row">
                                        <h6>{{ $item->peraturan ?? '' }}</h6>
                                        <p>Bab {{ $item->bab ?? '' }} - Pasal {{ $item->pasal ?? '' }}</p>
                                        <b>{{ $item->judul ?? '' }}</b>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    
</div>

@endsection
@endsection
