@extends('layouts.app-home')

@section('content')
<div class="container lawyer-home">
    <div class="row justify-content-center">
        <div class="col-md-12" style="padding-bottom: 3rem">
            <a href="/client">
                <div class="card">
                    <div class="card-header">
                        <h5>Daftar Klien</h5>
                    </div>
                </div>
            </a>
            <a href="/client/case">
                <div class="card">
                    <div class="card-header">
                        <h5>Catatan Pengacara</h5>
                    </div>
                </div>
            </a>
            <a onclick="ok.performClick();">
                <div class="card">
                    <div class="card-header">
                        <h5>Prediksi AI</h5>
                    </div>
                </div>
            </a>
            <a href="/prediksi/lawyer">
                <div class="card">
                    <div class="card-header">
                        <h5>Prediksi Harga Pengacara</h5>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>
{{-- <script language="javascript">

    function predictButton()
    {
       valid.performClick();
       document.getElementById("ok").value = "OK";
    }
 
 </script> --}}
@endsection
