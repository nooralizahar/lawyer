@extends('layouts.app')
@section('header')
<div class="container">
    <button class="navbar-toggler" onclick="history.back()">
        <i class="fa fa-arrow-left"></i>
    </button>
    <a class="navbar-brand">
        Profile
    </a>
</div>
@section('content')
<div class="container lawyer-content">
    <div class="row justify-content-center">
        <div class="col-md-12" style="padding-bottom: 3rem">
            <form action="/profile/update" method="POST">
                <div class="card">
                    <div class="card-header">
                        <h5>Profile</h5>
                    </div>
                    <div class="card-body">
                        <div class="modal-body">
                            @csrf
                            <div class="form-group">
                                <label class="col-form-label text-md-end" for="">Nama</label>
                                <input type="text" class="form-control" name="name"value="{{ Auth::user()->name ?? '' }}">
                            </div>
                            <div class="form-group">
                                <label class="col-form-label text-md-end" for="">Email</label>
                                <input type="email" class="form-control" name="email" value="{{ Auth::user()->email ?? '' }}">
                            </div>
                            <div class="form-group">
                                <label class="col-form-label text-md-end" for="">ID Pengacara</label>
                                <input type="text" class="form-control" name="id_lawyer" value="{{ Auth::user()->id_lawyer ?? '' }}">
                            </div>
                        </div>
                        
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    
</div>

@endsection
@endsection
