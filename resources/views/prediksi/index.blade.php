@extends('layouts.app')
@section('header')
<div class="container">
    <button class="navbar-toggler" onclick="history.back()">
        <i class="fa fa-arrow-left"></i>
    </button>
    <a class="navbar-brand">
        Prediksi Harga
    </a>
</div>
@php
    if (isset($kode_old)) {
        $kode = $kode_old;
        $jenis = $jenis_old;
    } else {
        $kode = "-1";
        $jenis = "-1";
    }
@endphp
@section('content')
<div class="container lawyer-content">
    <div class="row justify-content-center">
        <div class="col-md-12" style="padding-bottom: 3rem">
            <form action="/prediksi/lawyer/search" method="POST">
                <div class="card">
                    <div class="card-header">
                        <h5>Prediksi</h5>
                    </div>
                    <div class="card-body">
                        <div class="modal-body">
                            @csrf
                            <div class="form-group">
                                <label class="col-form-label text-md-end" for="">Lama Hukuman</label>
                                <select name="kode" id="" class="form-control" style="width: 100%" required>
                                    <option value="" selected disabled>Pilih</option>
                                    <option value="1" {{ $kode == '1' ? 'selected' : '' }}>< 2 thn</option>
                                    <option value="2" {{ $kode == '2' ? 'selected' : '' }}>2 - 5 thn</option>
                                    <option value="3" {{ $kode == '3' ? 'selected' : '' }}>5 - 10 thn</option>
                                    <option value="4" {{ $kode == '4' ? 'selected' : '' }}>> 10 thn</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="col-form-label text-md-end" for="">Jenis</label>
                                <select name="jenis" id="" class="form-control" style="width: 100%" required>
                                    <option value="" selected disabled>Pilih</option>
                                    <option value="1" {{ $jenis == '1' ? 'selected' : '' }}>Narkotika</option>
                                    <option value="2" {{ $jenis == '2' ? 'selected' : '' }}>Tindak Pidana Korupsi</option>
                                    <option value="3" {{ $jenis == '3' ? 'selected' : '' }}>Kejahatan terhadap Nyawa</option>
                                    <option value="4" {{ $jenis == '4' ? 'selected' : '' }}>Pencurian</option>
                                    <option value="5" {{ $jenis == '5' ? 'selected' : '' }}>Pemalsuan Mata Uang dan Uang Kertas</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="col-form-label text-md-end" for="">Jarak</label>
                                <input type="number" class="form-control" value="{{ $jarak_old ?? 0 }}" name="jarak" required autocomplete="jarak">
                            </div>
                            
                        </div>
                        
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Prediksi</button>
                    </div>
                </div>
            </form>
            @if (isset($result))
            <div class="card mt-3">
                <div class="card-header">
                    <h6>Hasil</h6>
                </div>
                <div class="card-body">
                    <b>
                        Rp. {{ round($result->prediction[0], 2) ?? 0 }} Juta
                    </b>
                </div>
            </div>
            @endif
        </div>
    </div>
    
</div>

@endsection
@endsection
