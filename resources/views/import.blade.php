<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <form class="card card-custom mt-3" action="/import/pasal" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="card-header">
            <div class="card-title">
                <span class="card-icon"><i class="flaticon2-delivery-package text-primary"></i></span>
                <h3 class="card-label">Upload Pasal</h3>
            </div>
        </div>
        <div class="card-body">
            <!--begin: form-->
            <div class="form-group row">
                <div class="col-lg-8">
                    <input type="file" accept=".xlsx, .xls" class="form-control" placeholder="Upload Report" name="file" required />
                    <span class="form-text text-muted">* Upload file dengan ekstension .xlsx, .xls</span>
                </div>
                <div class="col-lg-2" style="margin-top: 10px">
                    <input type="submit" class="btn btn-primary" style="" value="Import Excel">
                </div>
            </div>
            <!--end: form-->
        </div>
    </form>
</body>
</html>