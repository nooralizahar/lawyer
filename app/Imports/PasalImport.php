<?php

namespace App\Imports;

use App\Models\Pasal;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class PasalImport implements ToModel, WithHeadingRow
{
    public function model(array $row)
    {
        return new Pasal([
            'peraturan' => $row['peraturan'],
            'bab' => $row['bab'], 
            'judul' => $row['judul'], 
            'pasal' => $row['pasal'], 
            'isi_pasal' => $row['isipasal']
        ]);
    }
}
