<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\UserCore;
use App\Models\Client;
use App\Models\Cases;
use App\Models\Pasal;
use App\Models\CasesDetail;
use Validator;
use Redirect;

class PrediksiHargaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('prediksi.index');
    }
    public function search(Request $request)
    {
        $url = 'http://103.161.184.103/predict';
        $dataSending = Array();
        $dataSending['kode'] = $request->kode;
        $dataSending['jenis'] = $request->jenis;
        $dataSending['jarak'] = $request->jarak;

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($dataSending),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $get = json_decode($response);
        $data['result'] = $get;
        $data['kode_old'] = $request->kode;
        $data['jenis_old'] = $request->jenis;
        $data['jarak_old'] = $request->jarak;

        return view('prediksi.index', $data);
    }
}
