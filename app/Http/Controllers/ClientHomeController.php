<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\UserCore;
use App\Models\Client;
use App\Models\Cases;
use App\Models\CasesDetail;
use Validator;
use Redirect;

class ClientHomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data['client'] = Client::where('users_id', Auth::user()->id)->orderBy('id', 'DESC')->get();
        return view('client.client', $data);
    }
    
    public function save(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|between:2,100',
            'email' => 'required|string|email|max:100|unique:client',
            'phone_number' => ['required', 'phone:ID'],
            'address' => 'required|string|between:2,255',
        ]);
        $client = Client::create(array_merge(
            $validator->validated(),
            ['users_id' => Auth::user()->id]
        ));
        return Redirect::back();
    }

    public function index_case(Request $request)
    {
        $data['client'] = Client::where('users_id', Auth::user()->id)->orderBy('id', 'DESC')->get();
        if ($request->segment(3) == null) {
            $data['client_case'] = Cases::whereHas('client', function ($row) {
                $row->where('users_id', Auth::user()->id)->orderBy('id', 'DESC');
            })->get();
        }else{
            $data['client_case'] = Cases::where('client_id', $request->segment(3))->get();
        }
        return view('client.client-case', $data);
    }

    public function save_case(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'client_id' => 'required',
            'meeting_date' => 'required',
            'description' => 'required',
            'status' => 'required', // New, Pull Up, Continue, Dismiss
        ]);

        $cases = Cases::create(array_merge(
            $validator->validated(),
            ['status' => 'New']
        ));

        return Redirect::back();
    }
}
