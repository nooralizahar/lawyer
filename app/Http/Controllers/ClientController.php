<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Client;
use Validator;

class ClientController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:api');
    }
    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */

     public function index(Request $request)
     {
        $client = Client::where('users_id', auth('api')->user()->id)->get();
        return response()->json([
            'message' => 'Client list',
            'data' => $client
        ], 201);

     }

     public function store(Request $request)
     {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|between:2,100',
            'email' => 'required|string|email|max:100|unique:client',
            'phone_number' => ['required', 'phone:ID'],
            'address' => 'required|string|between:2,255',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        $client = Client::create(array_merge(
                    $validator->validated(),
                    ['users_id' => auth('api')->user()->id]
                ));
        return response()->json([
            'message' => 'Client successfully added',
            'data' => $client
        ], 201);
     }

     public function update(Request $request)
     {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|between:2,100',
            'email' => 'required|string|email|max:100|unique:client,email,'.$request->client_id,
            'phone_number' => ['required', 'phone:ID'],
            'address' => 'required|string|between:2,255',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        $update = Client::where('id', $request->client_id)->update(array_merge(
                    $validator->validated()
                ));
        $client = Client::where('id', $request->client_id)->first();
        return response()->json([
            'message' => 'Client successfully updated',
            'data' => $client
        ], 201);
     }

     public function get($client_id = null)
     {
        $client = Client::where('id', $client_id)->first();
        return response()->json([
            'message' => 'Detail client list',
            'data' => $client,
        ], 201);

     }

     public function delete($client_id = null)
     {
        $delete = Client::where('id', $client_id)->delete();
        $client = Client::where('users_id', auth('api')->user()->id)->get();
        return response()->json([
            'message' => 'Client successfully deleted',
            'data' => $client
        ], 201);

     }
}
