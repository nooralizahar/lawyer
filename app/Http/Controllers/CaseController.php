<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Cases;
use App\Models\CasesDetail;
use App\Models\CasesDocument;
use App\Models\CasesSettlement;
use App\Models\CasesSettlementDetail;
use App\Models\CasesSession;
use App\Models\Client;
use Validator;
use Redirect,Response,File, Storage;

class CaseController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:api');
    }
    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */

     public function index($client_id = null)
     {
        $cases = Cases::where('client_id', $client_id)->get();
        return response()->json([
            'message' => 'Case successfully showing',
            'data' => $cases
        ], 201);

     }

     public function store(Request $request)
     {
        $validator = Validator::make($request->all(), [
            'client_id' => 'required',
            'meeting_date' => 'required',
            'description' => 'required',
            'status' => 'required',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        $cases = Cases::create(array_merge(
                    $validator->validated()
                ));
        return response()->json([
            'message' => 'Case successfully added',
            'data' => $cases
        ], 201);
     }

     public function update(Request $request)
     {
        $validator = Validator::make($request->all(), [
            'meeting_date' => 'required',
            'description' => 'required',
            'status' => 'required',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        $update = Cases::where('id', $request->cases_id)->update(array_merge(
                    $validator->validated()
                ));
        $cases = Cases::where('id', $request->cases_id)->first();
        return response()->json([
            'message' => 'Cases successfully updated',
            'data' => $cases
        ], 201);
     }

     public function get($cases_id = null)
     {
        $cases = Cases::where('id', $cases_id)->first();
        $detail = CasesDetail::where('case_id', $cases_id)->first();
        $settlement = CasesSettlement::where('case_id', $cases_id)->first();
        $session = CasesSession::where('case_id', $cases_id)->get();
        $document = CasesDocument::where('case_id', $cases_id)->get();
        return response()->json([
            'message' => 'Detail cases list',
            'data' => [
                'cases' => $cases,
                'detail' => $detail,
                'settlement' => $settlement,
                'session' => $session,
                'document' => $document,
            ]
        ], 201);

     }

     public function delete($cases_id = null)
     {
        $delete = Cases::where('id', $cases_id)->delete();
        $delete_detail = CasesDetail::where('case_id', $cases_id)->delete();
        $delete_session = CasesSession::where('case_id', $cases_id)->delete();
        $delete_document = CasesDocument::where('case_id', $cases_id)->delete();
        $delete_settlement = CasesSettlement::where('case_id', $cases_id)->delete();
        $cases = Cases::where('users_id', auth('api')->user()->id)->get();
        return response()->json([
            'message' => 'Cases successfully deleted',
            'data' => $cases,
        ], 201);

     }

     public function storeDetail(Request $request)
     {
        $validator = Validator::make($request->all(), [
            'case_id' => 'required',
            'case_number' => 'required',
            'case_date' => 'required',
            'claim_detail' => 'required',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        $cases = CasesDetail::create(array_merge(
                    $validator->validated()
                ));
        return response()->json([
            'message' => 'Case detail successfully added',
            'data' => $cases
        ], 201);
     }

     public function updateDetail(Request $request)
     {
        $validator = Validator::make($request->all(), [
            'case_id' => 'required',
            'case_number' => 'required',
            'case_date' => 'required',
            'claim_detail' => 'required',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        $update = CasesDetail::where('id', $request->detail_id)->update(array_merge(
                    $validator->validated()
                ));
        $cases = CasesDetail::where('id', $request->detail_id)->first();
        return response()->json([
            'message' => 'Case detail successfully updated',
            'data' => $cases
        ], 201);
     }

     public function storeSettlement(Request $request)
     {
        $validator = Validator::make($request->all(), [
            'case_id' => 'required',
            'case_resolution' => 'required',
            'result' => 'required',
            'lawsuit_date' => '',
            'lawsuit_reason' => '',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        $cases = CasesSettlement::create(array_merge(
                    $validator->validated()
                ));
        return response()->json([
            'message' => 'Case settlement successfully added',
            'data' => $cases
        ], 201);
     }

     public function updateSettlement(Request $request)
     {
        $validator = Validator::make($request->all(), [
            'case_id' => 'required',
            'case_resolution' => 'required',
            'result' => 'required',
            'lawsuit_date' => 'required',
            'lawsuit_reason' => 'required',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        $update = CasesSettlement::where('id', $request->settlement_id)->update(array_merge(
                    $validator->validated()
                ));
        $cases = CasesSettlement::where('id', $request->settlement_id)->first();
        return response()->json([
            'message' => 'Case settlement successfully updated',
            'data' => $cases
        ], 201);
     }

     public function storeSession(Request $request)
     {
        $validator = Validator::make($request->all(), [
            'case_id' => 'required',
            'date_session' => 'required',
            'result' => 'required',
            'file' => 'required|mimes:jpg,jpeg,png,pdf|max:5048'
        ]);
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        $file = $request->file('file')->store('session', 'public');
        $store = CasesSession::create(array_merge(
                    $validator->validated(),
                    ['file' => basename($file)]
                ));
        $cases = CasesSession::where('case_id', $request->case_id)->orderBy('id', 'DESC')->get();
        return response()->json([
            'message' => 'Case session successfully added',
            'data' => $cases
        ], 201);
     }

     public function updateSession(Request $request)
     {
        $validator = Validator::make($request->all(), [
            'case_id' => 'required',
            'date_session' => 'required',
            'result' => 'required',
            'file' => 'required|mimes:jpg,jpeg,png,pdf|max:5048'
        ]);
        $file = $request->file('file')->store('session', 'public');
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        $update = CasesSession::where('id', $request->session_id)->update(array_merge(
                    $validator->validated(),
                    ['file' => basename($file)]
                ));
        $cases = CasesSession::where('case_id', $request->case_id)->orderBy('id', 'DESC')->get();
        return response()->json([
            'message' => 'Case session successfully updated',
            'data' => $cases
        ], 201);
     }

     public function storeDocument(Request $request)
     {
        $validator = Validator::make($request->all(), [
            'case_id' => 'required',
            'file' => 'required|mimes:jpg,jpeg,png,pdf|max:5048'
        ]);
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        $file = $request->file('file')->store('document', 'public');
        $store = CasesDocument::create(array_merge(
                    $validator->validated(),
                    ['file' => basename($file)]
                ));
        $cases = CasesDocument::where('case_id', $request->case_id)->orderBy('id', 'DESC')->get();
        return response()->json([
            'message' => 'Case document successfully deleted',
            'data' => $cases
        ], 201);
     }

     public function deleteDocument($document_id = null)
     {
        $get = CasesDocument::where('id', $document_id)->first();
        $delete = CasesDocument::where('id', $document_id)->delete();
        return response()->json([
            'message' => 'Case document successfully deleted',
            'data' => ''
        ], 201);

     }
}
