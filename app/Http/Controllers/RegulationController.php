<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\UserCore;
use App\Models\Client;
use App\Models\Cases;
use App\Models\Pasal;
use App\Models\CasesDetail;
use Validator;
use Redirect;

class RegulationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data['pasal'] = Pasal::orderBy('bab', 'ASC')->get();
        return view('regulation.index', $data);
    }

    public function detail($id = null)
    {
        $data['row'] = Pasal::where('id', $id)->first();
        return view('regulation.detail', $data);
    }

    public function search(Request $request)
    {
        $data['pasal'] = Pasal::orderBy('bab', 'ASC')->where('isi_pasal', 'LIKE', '%'.$request->keywoard.'%')->get();
        $data['keywoard'] = $request->keywoard ?? '';
        return view('regulation.search', $data);
    }
}
