<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Imports\PasalImport;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Maatwebsite\Excel\Facades\Excel;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function profile()
    {
        return view('profile');
    }
    
    public function profile_update(Request $request)
    {
        $post = User::find(Auth::user()->id);
            
        $post->name = $request->name;
        $post->email = $request->email;
        $post->id_lawyer = $request->id_lawyer;
        
        $post->save();

        return redirect()->back();
    }

    public function import()
    {
        return view('import');
    }

    public function upload_pasal()
    {
        Excel::import(new PasalImport,request()->file('file'));
             
        return redirect()->route('home')->with(['success' => 'Pasal berhasil diimport']);
    }
}
