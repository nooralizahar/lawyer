<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CasesDocument extends Model
{
    use HasFactory;
    protected $table = 'case_document';

    protected $fillable = [
        'case_id',
        'file',
    ];

    public function cases(){
        return $this->belongsTo(Cases::class, 'case_id', 'id');
    }
}
