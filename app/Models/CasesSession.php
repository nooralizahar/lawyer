<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CasesSession extends Model
{
    use HasFactory;
    protected $table = 'case_session';

    protected $fillable = [
        'case_id',
        'date_session',
        'result',
        'file',
    ];

    public function cases(){
        return $this->belongsTo(Cases::class, 'case_id', 'id');
    }
}
