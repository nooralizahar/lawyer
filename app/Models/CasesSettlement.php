<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CasesSettlement extends Model
{
    use HasFactory;
    protected $table = 'case_settlement';

    protected $fillable = [
        'case_id',
        'case_resolution',
        'result',
    ];

    public function cases(){
        return $this->belongsTo(Cases::class, 'case_id', 'id');
    }
}
