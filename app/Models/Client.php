<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;
    protected $table = 'client';

    protected $fillable = [
        'name',
        'email',
        'phone_number',
        'address',
        'users_id',
    ];

    public function lawyer(){
        return $this->belongsTo(User::class, 'users_id', 'id');
    }

    public function lawyer_core(){
        return $this->belongsTo(UserCore::class, 'users_id', 'id');
    }
}
