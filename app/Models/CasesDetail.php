<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CasesDetail extends Model
{
    use HasFactory;
    protected $table = 'case_detail';

    protected $fillable = [
        'case_id',
        'case_number',
        'case_date',
        'claim_detail',
    ];

    public function cases(){
        return $this->belongsTo(Cases::class, 'case_id', 'id');
    }
    
    public function settlement()
    {
        return $this->hasMany(CasesSettlement::class, 'case_detail_id', 'id');
    }
}
