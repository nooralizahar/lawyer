<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cases extends Model
{
    use HasFactory;
    protected $table = 'case';

    protected $fillable = [
        'client_id',
        'meeting_date',
        'description',
        'status',
    ];

    public function client(){
        return $this->belongsTo(Client::class, 'client_id', 'id');
    }
    
    public function detail()
    {
        return $this->hasMany(CasesDetail::class, 'case_id', 'id');
    }
    
    public function document()
    {
        return $this->hasMany(CasesDocument::class, 'case_id', 'id');
    }
    
    public function session()
    {
        return $this->hasMany(CasesSession::class, 'case_id', 'id');
    }
    
    public function settlement()
    {
        return $this->hasMany(CasesSettlement::class, 'case_id', 'id');
    }
}
