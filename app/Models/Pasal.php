<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pasal extends Model
{
    use HasFactory;
    protected $table = 'pasal';

    protected $fillable = [
        'peraturan',
        'bab',
        'judul',
        'pasal',
        'isi_pasal',
    ];
}
