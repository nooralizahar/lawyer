<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCaseSettlementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('case_settlement', function (Blueprint $table) {
            $table->id();
            $table->integer('case_id')->nullable();
            $table->string('case_resolution')->nullable();
            $table->string('result')->nullable();
            $table->Date('lawsuit_date')->nullable();
            $table->string('lawsuit_reason')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('case_settlement');
    }
}
