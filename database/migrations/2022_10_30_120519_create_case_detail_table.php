<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCaseDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('case_detail', function (Blueprint $table) {
            $table->id();
            $table->integer('case_id')->nullable();
            $table->string('case_number')->nullable();
            $table->Date('case_date')->nullable();
            $table->string('claim_detail')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('case_detail');
    }
}
